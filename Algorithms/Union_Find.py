import numpy as np

class Union_Find():
    def __init__(self, binaryImage_):
        #print("Starting union find on array of shape", np.shape(binaryImage_))
        self.binaryImage = binaryImage_
        self.LB = np.zeros((np.shape(binaryImage_)[0], np.shape(binaryImage_)[1]))

        self.label = 1
        self.parent = [0 for i in range(1, len(binaryImage_.flatten()))]

    def find(self, label):
        dummy = int(label)
        while self.parent[dummy] != 0: dummy = self.parent[dummy]
        return dummy

    def union(self, a, b):
        a_ = int(a)
        b_ = int(b)
        a_size = 0
        b_size = 0
        while self.parent[a_] != 0:
            a_ = self.parent[a_]
            a_size += 1
        while self.parent[b_] != 0:
            b_ = self.parent[b_]
            b_size += 1
        if a_size < b_size:
            c_ = a_
            a_ = b_
            b_ = c_

        if a_ != b_:
            self.parent[b_] = a_


    def prior_neighbours(self, y, x):
        results = []
        if x > 0:
            if self.binaryImage[y][x-1] == 1:
                results.append([y,x-1])
        if y > 0:
            if self.binaryImage[y-1][x] == 1:
                results.append([y-1,x])
        if x > 0 and y > 0:
            if self.binaryImage[y-1][x-1] == 1:
                results.append([y-1,x-1])
        return results

    def labels(self, y, x):
        return self.LB[y][x]

    def stabilise_labels(self):
        labels = [0]
        for i_row in range(np.shape(self.LB)[0]):
            for i_column in range(np.shape(self.LB)[1]):
                if not (self.LB[i_row][i_column] in labels):
                    labels.append(self.LB[i_row][i_column])
                self.LB[i_row][i_column] = labels.index(self.LB[i_row][i_column])
        #print(len(labels) - 1, "different regions found")


    def run_classical_union_find(self):
        # Pass 1
        for i_row in range(np.shape(self.binaryImage)[0]):
            for i_column in range(np.shape(self.binaryImage)[1]):
                if self.binaryImage[i_row][i_column] == 1:
                    dummyLabels = []
                    adjacent = self.prior_neighbours(i_row, i_column)
                    if len(adjacent) == 0:
                        assignedLabel = self.label
                        self.label += 1
                    else:
                        for i_adjacent in range(len(adjacent)):
                            dummyLabels.append(self.labels(adjacent[i_adjacent][0], adjacent[i_adjacent][1]))
                        assignedLabel = min(dummyLabels)
                    self.LB[i_row][i_column] = assignedLabel
                    for x in dummyLabels:
                        if x != assignedLabel: self.union(assignedLabel, x)
        # Pass 2
        for i_row in range(np.shape(self.binaryImage)[0]):
            for i_column in range(np.shape(self.binaryImage)[1]):
                if self.binaryImage[i_row][i_column] == 1:
                    self.LB[i_row][i_column] = self.find(self.LB[i_row][i_column])

        self.stabilise_labels()
        return self.LB
