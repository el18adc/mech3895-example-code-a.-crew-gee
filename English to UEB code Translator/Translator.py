# word class | allows words to be translated with numeric and capital markers added
# text is lowered put into lowercase once it is initiated
# Contains:
# text - string of text added at init
# binary_6bit - array of 6 bit binary values corresponding to binary top->down, left->right | 1 is up, 0 is down
class Word:
    offsetToBinaryConversion_6bit = [32,48,36,38,34,52,54,50,20,22,40,56,44,46,42,60,62,58,28,30,41,57,35,45,47,43]
    validLetters = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
                    'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                    '1','2','3','4','5','6','7','8','9','0']

    def __init__(self, text_):
        self.binary_6bit = []
        self.specialPrefix = 0
        self.text = text_

        self.numericPrefix, self.numericInstances = self.find_prefix_instances(self.validLetters[52:])
        self.alphabeticalPrefix, self.capitalLetterInstances = self.find_prefix_instances(self.validLetters[26:52])

        self.generate_special_character_list()

        self.text = self.text.lower()

    # finds instances of items from array in text
    # returns array of instances as well as a T/F based off if the whole word fulfils the former requirement
    def find_prefix_instances(self, validArray):
        dummyInstances = []
        for i in range(len(self.text)):
            if (self.text[i] in validArray):
                dummyInstances.append(i)
        if (len(dummyInstances) == len(self.text)):
            return True, dummyInstances
        else:
            return False, dummyInstances

    # converts lowered text to binary values then adds alphabetic and numeric indicators
    def convert_to_binary(self):
        # convert text
        for i_letter in range(len(self.text)):
            if (self.specialCharacters[0][i_letter]):
                self.binary_6bit.append(self.specialCharacters[1][i_letter])
            else:
                self.binary_6bit.append(self.offsetToBinaryConversion_6bit[self.validLetters.index(self.text[i_letter]) % 26])

        # add numeric and alphabetic prefixes
        if (self.numericPrefix): self.add_indicators(15, 1,[0], [])
        elif (self.alphabeticalPrefix): self.add_indicators(15, 1,[], [0,0])
        else: self.add_indicators(15, 1, self.numericInstances, self.capitalLetterInstances)

        # add special prefix for initial letter contractions
        if self.specialPrefix != 0:
            if self.numericPrefix: self.binary_6bit.insert(1, self.specialPrefix)
            elif 0 in self.capitalLetterInstances: self.binary_6bit.insert(1, self.specialPrefix)
            else: self.binary_6bit.insert(0, self.specialPrefix)

        return self.binary_6bit

    # if text is contained within validWords it is replaced with special characters
    def replace_complete_word_with_special_characters(self, validWords, replacement, specialCharactersConversion):
        if (self.text in validWords):
            offset = validWords.index(self.text)
            self.text = replacement * len(specialCharactersConversion[0])
            self.generate_special_character_list()

            for i in range(len(self.text)):
                self.specialCharacters[0][i] = True
                self.specialCharacters[1][i] = specialCharactersConversion[offset][i]

    # if partial word found replace with contraction and updates special characters
    def replace_partial_contractions_with_special_characters(self, partialContraction, replacement, specialCharactersConversion):
        if (partialContraction in self.text):
            pos = self.text.index(partialContraction)
            length = len(partialContraction)
            # remove characters
            for i in range(length):
                self.text = self.remove_string_item(pos, self.text)
                self.specialCharacters[0].pop(pos)
                self.specialCharacters[1].pop(pos)
            # add replacement
            for i in range(len(specialCharactersConversion)):
                self.text = self.insert_item_into_string(i + pos, self.text, replacement)
                self.specialCharacters[0].insert(i + pos, True)
                self.specialCharacters[1].insert(i + pos, specialCharactersConversion[i])

    def insert_item_into_string(self, index, string, item):
        return string[:index] + item + string[index:]

    def remove_string_item(self, index, string):
        return string[:index] + string[index+1:]

    # insert either indicator a or b depending on the set instances a or b
    def add_indicators(self, indicator_a, indicator_b, instances_a, instances_b):
        changes = []
        for i in range(len(self.binary_6bit)):
            if (i in instances_a):
                changes.append([i, indicator_a])
            elif(i in instances_b):
                changes.append([i, indicator_b])

        offset = 0
        for i in range(len(changes)):
            self.binary_6bit.insert(changes[i][0] + offset,changes[i][1])
            offset+= 1

    # if word is contained within validWords then the corresponding replacement text is switched in
    def replace_word(self, validWords, replacements):
        dummyList = validWords + [self.text]
        if (dummyList.index(self.text) != len(validWords)):
            self.text = replacements[validWords.index(self.text)]
            self.generate_special_character_list()
            return True
        return False

    # if word is contained within validWords then it contracted and special characters and prefixes are updated
    def contract_word_with_special_prefix_and_special_character(self, validWords, contraction, specialPrefix, specialCharacterConversion):
        if (self.text in validWords):
            pos = validWords.index(self.text)
            self.text = contraction
            self.generate_special_character_list()
            self.add_special_character(0, specialCharacterConversion[pos])
            self.specialPrefix = specialPrefix
            return True
        return False

    # inserts special character into special character array
    def add_special_character(self, instance, value):
        self.specialCharacters[0][instance] = True
        self.specialCharacters[1][instance] = value

    # creates false special character list for length of text
    def generate_special_character_list(self):
        self.specialCharacters = [[],[]]
        # run through all text
        for i in range(len(self.text)):
            self.specialCharacters[0].append(False)
            self.specialCharacters[1].append(0)


class Translation_Module:
    def __init__(self):
        print("Initiating Translator")
        print("Creating internal phonetic dictionary")
        file = open("Phonetic_Dictionary.txt", "r", encoding = 'utf-8', errors = 'ignore')
        self.phoneticDictionary = {}
        for dummyLine in file:
            if (',' in dummyLine):
                pos = dummyLine.index(',')
                dummyLine = dummyLine[pos + 1:]
                pos = dummyLine.index(',')
                dummyWord = dummyLine[2:pos-1]
                self.phoneticDictionary[dummyWord] = dummyLine[pos+4:-4]
        file.close()
        print("Phonetic dictionary created with length:", len(self.phoneticDictionary))


    def translate(self, text):
        print("Translating", text)
        test = self.remove_untranslatable_characters(text)

        self.find_seperate_words(text, " ")
        self.create_word_array(text)

        # process complete words
        self.contract_alphabetic_wordsigns()
        self.contract_shortform_words()
        self.contract_strong_wordsigns()

        # access phonetic dictionary
        self.contract_strong_groupsigns()

        # partial word contractions
        self.contract_lower_wordsigns()
        self.contract_initial_letter_words()


        # process partial words
        self.contract_final_letter_groupsigns()
        self.contract_lower_groupsigns()
        self.contract_strong_contractions()

        #self.show_all_words()
        return self.generate_output()

    def remove_untranslatable_characters(self, input):
        translatableCharacters = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
                                  'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                                  '1','2','3','4','5','6','7','8','9','0']

        for i in range(len(input)):
            if not(input[i] in translatableCharacters):
                print("found", input[i])

    # returns 2d array of binary outputs
    def generate_output(self):
        results = []
        for i in range(self.wordCount):
            results.append(self.WordArray[i].convert_to_binary())
        return results

    # checks certain sounds against a phonetic dictionary
    # complete words that are translated are removed to stop overlap
    # shortform words with phonetic contractions are added
    def contract_strong_groupsigns(self):
        strongGroupsigns = ["ch","sh","th","wh","ou","st","gh","ed","er","ow","ar","ing"]
        phoneticIdentifiers = [["tʃ"], ["ʃ"], ["θ", "ð"], ["w"], ["u"], ["st"], ["g"], ["id", "d"], ["ər"], ["au"], ["a:"], ["iŋ"]]
        strongGroupsigns_conversion = [[33],[37],[39],[35],[51],[12],[49],[53],[55],[21],[14],[13]]
        # for all words
        for i_word in range(self.wordCount):
            dummyWord = self.WordArray[i_word]
            # check if in dictionary
            if dummyWord.text in self.phoneticDictionary:
                # check if explicit strong groupsign exists
                for i_groupsign in range(len(strongGroupsigns)):
                    if strongGroupsigns[i_groupsign] in dummyWord.text:
                        # it is contained so phonetic must be checked
                        phonetic = self.phoneticDictionary[dummyWord.text]
                        # is phonetic identifier in dictionary
                        phoneticIdentifier = phoneticIdentifiers[i_groupsign]
                        for i_type in range(len(phoneticIdentifier)):
                            if phoneticIdentifier[i_type] in phonetic:
                                # this means it is a valid interchange
                                dummyWord.replace_partial_contractions_with_special_characters(strongGroupsigns[i_groupsign],
                                                                                               " ",
                                                                                               strongGroupsigns_conversion[i_groupsign])
                                break

    # implement strong contractions
    def contract_strong_contractions(self):
        strongContractions = ["and","for","of","the","with","be","con","dis","en","in"]
        strongContractions_replacement = [[61],[63],[59],[29],[31],[24],[18],[19],[17],[10]]
        for i_word in range(self.wordCount):
            for i_contraction in range(len(strongContractions)):
                self.WordArray[i_word].replace_partial_contractions_with_special_characters(strongContractions[i_contraction], " ", strongContractions_replacement[i_contraction])

    # contract lower groupsigns
    def contract_lower_groupsigns(self):
        lowerGroupsigns = ["ea","bb","cc","ff","gg"]
        lowerGroupsigns_replacement = [[16],[24],[18],[26],[27]]
        for i_word in range(self.wordCount):
            for i_contraction in range(len(lowerGroupsigns)):
                self.WordArray[i_word].replace_partial_contractions_with_special_characters(lowerGroupsigns[i_contraction], " ", lowerGroupsigns_replacement[i_contraction])

    # contract strong wordsigns
    def contract_strong_wordsigns(self):
        strongWordsigns = ["child","shall","this","which","out","still"]
        strongWordsigns_conversion = [[33],[37],[39],[35],[51],[12]]
        for i_word in range(self.wordCount):
            self.WordArray[i_word].replace_complete_word_with_special_characters(strongWordsigns, " ", strongWordsigns_conversion)

    # contract lower wordsigns
    def contract_lower_wordsigns(self):
        lowerWordsigns = ["be","enough","were","his","in","was"]
        lowerWordsigns_conversion = [[24],[17],[27],[25],[10],[11]]
        for i_word in range(self.wordCount):
            for i_conversion in range(len(lowerWordsigns)):
                self.WordArray[i_word].replace_complete_word_with_special_characters(lowerWordsigns, " ", lowerWordsigns_conversion)

    # contracts final lettergroupsigns with special characters
    def contract_final_letter_groupsigns(self):
        finalLetterContractions = ["ound","ance","sion","less","ount","ence","ong","ful","tion","ness","ment","ity"]
        finalLetterContractions_replacement = [[5,38],[5,34],[5,46],[5,28],[5,30],[3,34],[3,54],[3,56],[3,46],[3,28],[3,30],[3,47]]
        for i_word in range(self.wordCount):
            for i_contraction in range(len(finalLetterContractions)):
                self.WordArray[i_word].replace_partial_contractions_with_special_characters(finalLetterContractions[i_contraction], " ", finalLetterContractions_replacement[i_contraction])

    # contract initial letters and add special prefix
    def contract_initial_letter_words(self):
        initialLetterWords = [["day","ever","father","here","know","lord","mother","name","one","part","question","right","some","time","under","work","young","there","character","through","where","ought"],
                              ["upon","word","these","those","whose"],
                              ["cannot","had","many","spirit","world","their"]]
        initialLetterPrefixes = [2,6,7]
        initialLetterConversions = [[38,34,52,50,40,56,44,46,42,60,62,58,28,30,41,35,47,29,33,39,35,51],
                                    [41,23,29,39,35],
                                    [36,50,44,28,23,29]]

        for i_word in range(self.wordCount):
            for i_conversionType in range(3):
                if (self.WordArray[i_word].contract_word_with_special_prefix_and_special_character(initialLetterWords[i_conversionType],
                                                                                                   " ",
                                                                                                   initialLetterPrefixes[i_conversionType],
                                                                                                   initialLetterConversions[i_conversionType])):
                    break;

    # contract shortform words
    def contract_shortform_words(self):
        shortformWords = ["about","above","according","across","after","afternoon","afterward","again","against","almost","already","also","although","altogether","always","because","before","behind","below","beneath","beside","between","beyond","blind","braille","children","conceive","conceiving","could","decieve","deceiving","declare","declaring","either","first","friend","good","great","herself","him","himself","immediate","its","itself","letter","little","much","must","myself","necessary","neither","oneself","ourselves","paid","perceive","perceiving","perhaps","quick","recieve","receiving","rejoice","rejoicing","said","should","such","themselves","thyself","today","together","tomorrow","tonight","would","your","yourself","yourselves"]
        shortformWords_conversion = ["ab","abv","ac","acr","af","afn","afw","ag","agst","alm","alr","al","alth","alt","alw","bec","bef","beh","bel","ben","bes","bet","bey","bl","brl","chn","concv","concvg","cd","dcv","dcvg","dcl","dclg","ei","fst","fr","gd","grt","herf","hm","hmf","imm","xs","xf","lr","ll","mch","mst","myf","nec","nei","onef","ourvs","pd","percv","percvg","perh","qk","rcv","rcvg","rjc","rjcg","sd","shd","sch","themvs","thyf","td","tgr","tm","tn","wd","yr","yrf","yrvs"]

        for i in range(self.wordCount):
            self.WordArray[i].replace_word(shortformWords, shortformWords_conversion)

    # contract alphabetic wordsigns
    def contract_alphabetic_wordsigns(self):
        alphabeticWordsigns = ["but","can","do","every","from","go","have","just","knowledge","like","more","not","people","quite","rather","so","that","us","very","will","it","you","as"]
        alphabeticWordsigns_conversion = ["b","c","d","e","f","g","h","j","k","l","m","n","p","q","r","s","t","u","v","w","x","y","z"]

        for i in range(self.wordCount):
            self.WordArray[i].replace_word(alphabeticWordsigns, alphabeticWordsigns_conversion)

    # creates public 2 dimensional array of start and ends of words once split by seperator
    def find_seperate_words(self, input, seperator):
        # words is a 2D array of start/end | [instance of first letter, instance of final letter]
        self.wordsStartEnd = []
        dummyStartEnd = [0, None]
        for i in range(len(input)):
            if (input[i] == seperator):
                dummyStartEnd[1] = i - 1
                self.wordsStartEnd.append(dummyStartEnd[:])
                dummyStartEnd[0] = i + 1
        self.wordsStartEnd.append([dummyStartEnd[0], len(input)])

    # creates array of words
    def create_word_array(self, input):
        self.WordArray = []
        dummyWord = Word("")
        for i in range(len(self.wordsStartEnd)):
            dummyWord = Word(input[self.wordsStartEnd[i][0]:self.wordsStartEnd[i][1] + 1])
            self.WordArray.append(dummyWord)
        self.wordCount = len(self.WordArray)    # word count used for brevity within code

    # display function that shows all words
    def show_all_words(self):
        for i in range(len(self.WordArray)):
            self.show_word(i)

    # shows specific word and its translation
    def show_word(self, i):
        if (i < len(self.WordArray)): print(self.WordArray[i].text, "|", self.WordArray[i].convert_to_binary())
        else: print("Instance", i, "is out of range with word array of length", len(self.WordArray))