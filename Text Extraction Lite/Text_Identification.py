# External Libraries
from PIL import Image
import numpy as np

# Internal Libraries
import Union_Find as union
import Letter_Stamps as stamp
import Text_Conversion

# convert line boundaries to image
def convert_boundaries_to_image(boundaries, image, scaler):
    output = []
    for i in range(len(boundaries)):
        lowerRow = int(boundaries[i][0][0] * scaler)
        upperRow = int(boundaries[i][0][1] * scaler)
        lowerColumn = int(boundaries[i][1][0] * scaler)
        upperColumn = int(boundaries[i][1][1] * scaler)
        image = image[lowerRow:upperRow, lowerColumn:upperColumn]

    return output

# returns the bounding lines for possible lines within an image
def find_text_in_image(image, numberOfThresholds):
    print("Initialising neural network...")
    MLPsizes = [1500, 500, 75]  # MLP neuron count - hidden layer 1, hidden layer 2, output layer
    MLPbeta = 50  # MLP beta value
    sizes = [1024, MLPsizes[0], MLPsizes[1], MLPsizes[2]]
    network = Text_Conversion.Multilayer_Perceptron(sizes, MLPbeta, 0.9)
    network.set_weights([Text_Conversion.convert_file_to_2D_array("Input_Files/Weights_1.txt"),
                         Text_Conversion.convert_file_to_2D_array("Input_Files/Weights_2.txt"),
                         Text_Conversion.convert_file_to_2D_array("Input_Files/Weights_3.txt")])
    print("Finished initialising network of size", sizes)
    print("Finding text in image of size", np.shape(image), "by using", numberOfThresholds, "thresholds")
    # ---------- find corners across multiple thresholds ----------
    thresholdList = create_threshold_images(image, numberOfThresholds)  # create list of threshold arrays
    harrisResponseList = create_harris_response_images(thresholdList)  # create list of harris response images

    edgeArray = detect_anchor_points(harrisResponseList, 60)  # create summed array of edges

    # ---------- find densities to create lines ----------
    rowSum = np.sum(edgeArray, axis=0)
    columnSum = np.sum(edgeArray, axis=1)

    # create array of lines and return local average
    averageWindow = int(np.shape(image)[0] / 200)
    linePeaks, localAverageArray = identify_peaks(columnSum, averageWindow, True,"Positive")
    lineMinima, dummyArray = identify_peaks(np.absolute(np.max(columnSum) - columnSum), averageWindow, False,"Negative")
    #constrain lines by minima
    lineBoundaries = np.array(constrain_peaks_by_minima(linePeaks, lineMinima))

    print("Found", np.shape(lineBoundaries)[0], "lines")
    # for all lines...
    completeText = []
    for i in range(np.shape(lineBoundaries)[0]):
        # ---------- Seperate lines by row instensity ----------
        # create 'strip' within constraints
        stripImage = image[lineBoundaries[i][0]:lineBoundaries[i][1],:]
        stripEdge = edgeArray[lineBoundaries[i][0]:lineBoundaries[i][1],:]
        print("Constraining line", i, "of original shape", np.shape(stripEdge))

        rowSum = np.sum(stripEdge, axis=0)
        localAverage = create_local_average(rowSum, 10)

        # Skim bottom 20% of limits
        maximum = np.max(localAverage)
        localAverage = np.where(localAverage > (maximum * 0.2), localAverage, 0)
        nonZero = np.where(localAverage != 0)
        constrainedImage = image[lineBoundaries[i][0]:lineBoundaries[i][1],nonZero[0][0]:nonZero[0][-1]]
        
        print("Created constrained image of shape", np.shape(constrainedImage))
        print("Beginning letter seperation of line", i)
        thresholdImage = np.where(constrainedImage > create_local_mean(constrainedImage), 1, 0)
        dummyUnion = union.Union_Find(thresholdImage)
        labelImage = dummyUnion.run_classical_union_find()

        print("Finished union find on line", i)
        stampArray = create_stamps(labelImage)
        print("Created", len(stampArray), "stamps")
        print("Beginning identification of text")

        text = ""
        for i_stamp in range(0, len(stampArray)):
            dummyImage = stampArray[i_stamp].image
            out = pytesseract.image_to_string(Image.fromarray(np.uint8(dummyImage * 255)).convert('RGB'))
            if i_stamp == 0:
                text += out
                continue
            elif stampArray[i_stamp].minColumn - stampArray[i_stamp - 1].maxColumn > 5:
                text+= " "
            text += out
            
            dummySample = Text_Conversion.create_sample_from_array(dummyImage * 255)
            out = network.run([dummySample])
            if np.max(out) > 0.2:
                if stampArray[i_stamp].minColumn - stampArray[i_stamp - 1].maxColumn > 5: text += " "
                text += chr(np.argmax(out) + 48)
        print("Translated line", i)
        print(text)
        completeText.append(text)
    return completeText


def create_stamps(labelImage):
    print("Creating stamps")
    height = np.shape(labelImage)[0] / 2
    print("Line centre line:", height)
    labels = np.unique(labelImage)
    labels = labels[labels != 0]
    print(len(labels), "unique, non-zero, labels discovered")
    stamps = []
    rowConstraints = [int(height/2), int(height * 1.6)]
    columnConstraints = [int(height/8), int(height * 1.6)]
    print("Using letter constraints", rowConstraints, "by row |", columnConstraints, "by column")
    for i_label in range(len(labels)):
        dummyStamp = stamp.Stamp(np.where(labelImage == labels[i_label]))
        if abs(height - dummyStamp.row) < height * 3/4 and dummyStamp.validate_stamp(rowConstraints,columnConstraints):
            dummyStamp.create_image()
            dummyStamp.correct_image()
            dummyUnion = union.Union_Find(dummyStamp.image)
            secondaryLabelImage = dummyUnion.run_classical_union_find()
            secondaryLabels = np.unique(secondaryLabelImage)
            secondaryLabels = secondaryLabels[secondaryLabels != 0]
            for i_new in range(len(secondaryLabels)):
                newDummyStamp = stamp.Stamp(np.where(secondaryLabelImage == secondaryLabels[i_new]))
                if newDummyStamp.validate_stamp(rowConstraints,columnConstraints):
                    newDummyStamp.create_image()
                    #newDummyStamp.dilate_image()
                    newDummyStamp.offset_values(dummyStamp.minRow, dummyStamp.minColumn)
                    stamps.append(newDummyStamp)
                    
    print(len(stamps), "valid stamps found")
    stamps = order_stamps_by_column(stamps)
    return stamps

def order_stamps_by_column(stamps):
    return sorted(stamps, key = lambda x: x.column)

def create_local_mean(image):
    image_3D = create_3D_image(image)
    return np.mean(image_3D, axis=2)

def create_3D_image(image):
    image_3D = image[:, :, np.newaxis]
    shiftType = ['up_left', 'up', 'up_right', 'right', 'down_right', 'down', 'down_left', 'left']
    # create 3 dimensional array of wobbled image in all directions
    for i in range(8):
        image_3D = np.dstack((image_3D, shift_array(image, shiftType[i])))
    return image_3D

def remove_small_labels(labelImage):
    unique, counts = np.unique(labelImage, return_counts=True)
    counts = counts[unique != 0]
    unique = unique[unique != 0]
    average = np.sum(counts)/len(counts)
    standardDeviation = (np.sum((counts-average) ** 2)/len(unique))**0.5
    for i in range(len(unique)):
        labelImage[labelImage == unique[i]] = counts[i]
    return np.where(labelImage > average + (0.05 *standardDeviation), labelImage, 0)

def find_2D_local_average(image):
    # create 'wobbled' image
    image_3D = image[:, :, np.newaxis]
    shiftType = ['up','right','down','left']
    # create 3 dimensional array of wobbled image in all directions
    for i in range(len(shiftType)):
        image_3D = np.dstack((image_3D, shift_array(image, shiftType[i])))
    image_3D[0][:] = 255
    image_3D[-1][:] = 255
    image_3D[0][:] = 255
    image_3D[-1][:] = 255

    averageImage = np.mean(image_3D, axis=2)

    return averageImage

# constrains peaks by a minima
def constrain_peaks_by_minima(peaks, minima):
    lines = []
    for i in range(np.shape(peaks)[1]):
        # higher values -> pos, lowerValues -> neg
        dummyDifference = minima - peaks[0][i]
        dummyNeg = dummyDifference[dummyDifference < 0]
        dummyPos = dummyDifference[dummyDifference > 0]
        # for upper bound get smallest positive value
        if len(dummyPos) == 0: continue
        else:                  upperPoint = peaks[0][i] + np.min(dummyPos)
        # for lower bound get largest negative value
        if len(dummyNeg) == 0: continue
        else:                  lowerPoint = peaks[0][i] + np.max(dummyNeg)
        lines.append([lowerPoint, upperPoint])
    return lines

# Creates a local average array from flat array
def create_local_average(array, windowSize):
    length = np.shape(array)[0]
    # pad zeros on ends
    paddedArray = np.zeros((length + (2 * windowSize)))
    paddedArray[windowSize:length + windowSize] = array
    # create local averages of p +- windowSize
    localAverageArray = np.zeros((length))
    for i in range(length):
        localAverageArray[i] = np.sum(paddedArray[i:i + (2 * windowSize)]) / (2 * windowSize)

    return localAverageArray

# Identifies peaks in array of values
def identify_peaks(array, windowSize, cullPeaks, fileName):
    localAverageArray = create_local_average(array, windowSize)

    #histogram = qol.create_histogram(localAverageArray, 200)
    #qol.save_image_from_array(histogram, "Output_Files/Histograms/"+fileName+"_Local_Average_Histogram", "proportional")
    # create array of no greater value within window from window rolling average array
    length = np.shape(array)[0]
    paddedArray = np.zeros((length + (2*windowSize)))
    paddedArray[windowSize:length + windowSize] = localAverageArray
    peakArray = np.zeros((length))
    peakSum = 0
    justPeaksArray = []
    for i in range(length):
        if localAverageArray[i] == np.max(paddedArray[i:i+(2*windowSize)]):
            peakArray[i] = localAverageArray[i]
            peakSum += localAverageArray[i]
            justPeaksArray.append(localAverageArray[i])

    # find distribution and average of peaks
    count = len(justPeaksArray)
    average = peakSum / count
    standardDeviation = (np.sum(((justPeaksArray - average) ** 2)) / count) ** 0.5
    print("Found", count, "peaks with: average", average,"| standard deviation", standardDeviation)

    # cull peaks that don't fall in 3SD
    if cullPeaks:
        peakArray = np.where(peakArray >= average - (0.8 * standardDeviation), peakArray, 0)

    return np.where(peakArray > 0), localAverageArray

# creates 'n' threshold arrays
def create_threshold_images(array, n):
    step = 255 / (n + 1)
    threshold = step
    bufferList = []
    for i in range(n):
        threshold += step
        bufferList.append(np.where(array >= threshold, 1, 0))

    return bufferList

# creates 1/0 binary array based off if 1D intensity lies above threshold
def create_threshold(array, threshold):
    return np.where(array >= threshold, 1, 0)

# performs the sum of squared differences in a 3 by 3 window around every pixel
def sum_of_squared_differences(image, sobelDerivative):
    # create 'wobbled' image
    image_3D = image[:,:,np.newaxis]
    shiftType = ['up_left','up','up_right','right','down_right','down','down_left','left']
    # create 3 dimensional array of wobbled image in all directions
    for i in range(8):
        image_3D = np.dstack((image_3D, shift_array(image, shiftType[i])))

    # create 3D array of sobel derivative
    dummyArray = np.zeros((1,1,9))
    dummyArray[0][0] = sobelDerivative
    dummyIntensity = np.zeros((np.shape(image_3D)[0],np.shape(image_3D)[1]))
    # multiply each pixel of the image (9 dimensions) by the 9 dimensional sobel derivative
    dummyIntensity = np.sum(image_3D * dummyArray, axis=2)
    # Remove edges of intensity as the padding is not needed any more
    intensity = dummyIntensity[1:-1,1:-1]
    return intensity

# returns harris response array for all images in 'array'
def create_harris_response_images(array):
    dummyList = []
    for i in range(len(array)):
        print("Finding Harris Response for threshold ", i)
        dummyList.append(find_harris_response(array[i], 0.05))

    return dummyList

# returns sum of edges (harris response > threshold) over all thresholds
def detect_anchor_points(list, threshold):
    thresholdList = []
    anchorList = []
    # create array of 1/0 for every harris response where the value lies over the threshold
    for i in range(len(list)):
        thresholdList.append(np.where(list[i] >= threshold, 1, 0))

    # sums anchor points over all thresholds
    possibleAnchorPoints = np.zeros((np.shape(thresholdList[0])))
    for i in range(len(thresholdList)):
        possibleAnchorPoints += thresholdList[i]

    return possibleAnchorPoints

# finds harris response of an array
def find_harris_response(image, k):
    sobelDerivative_x = [0,1,2,1,0,-1,-2,-1,0]  # sobel derivative in x direction | [[1, 2, 1], [0, 0, 0], [-1, -2, -1]]
    sobelDerivative_y = [0,1,0,-1,-2,-1,0,1,2]  # sobel derivative in y direction | [[1, 0, -1], [2, 0, -2], [1, 0, -1]]


    pad = pad_image(image)  # pad image
    intensity_x = sum_of_squared_differences(pad, sobelDerivative_x)
    intensity_y = sum_of_squared_differences(pad, sobelDerivative_y)

    intensity_x_2 = intensity_x * intensity_x   # find intensityX ^ 2
    intensity_y_2 = intensity_y * intensity_y   # find intensityY ^ 2

    response = np.zeros((np.shape(image)))  # create response array
    # find response value for all values
    response = (intensity_x_2 * intensity_y_2) - (k * ((intensity_x_2 + intensity_y_2)**2))

    return response

# adds rim of zeros around array as padding
def pad_image(image):
    dummyImage = np.zeros((np.shape(image)[0] + 2, np.shape(image)[1] + 2))
    dummyImage[1 : np.shape(image)[0] + 1, 1 : np.shape(image)[1] + 1] = image
    return dummyImage

# shifts array by 1 in any direction
def shift_array(array, type):
    shifted = np.zeros((np.shape(array)))
    if type == 'up_right': shifted[:-1,1:] = array[1:,:-1]    # shift up right
    elif type == 'up': shifted[:-1,:] = array[1:,:]     # shift up
    elif type == 'up_left': shifted[:-1,:-1] = array[1:,1:]       # shift up left
    elif type == 'left': shifted[:,:-1] = array[:,1:]
    elif type == 'down_left': shifted[1:,:-1] = array[:-1,1:]
    elif type == 'down': shifted[1:,:] = array[:-1,:]
    elif type == 'down_right': shifted[1:,1:] = array[:-1,:-1]
    elif type == 'right': shifted[:,1:] = array[:,:-1]

    return shifted
