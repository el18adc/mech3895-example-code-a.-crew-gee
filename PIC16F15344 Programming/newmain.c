/*
 * File:   newmain.c
 * Author: Archi
 *
 * Created on 30 March 2021, 09:48
 */


#include <xc.h>
#pragma config RSTOSC = HFINT32 // 32MHz clock source
#pragma config WDTE = OFF       // WDT operating mode (WDT Disabled, SWDTEN is ignored)
#include <pic16f15344.h>

#define OR |

#define timer_0 T0CON0bits

#define timePeriod 20
#define maximumRotation 15
#define slowDistance 6
#define slowDR (timePeriod/10 * 3)
#define fastDR (timePeriod/10 * 5)

#define bounceTime 15000

// motor drive structure //

struct MotorDrive{
    unsigned int dutyRatio;
    signed char currentOrientation;
    signed char targetOrientation;
};

void increment_rotation(struct MotorDrive *motor, unsigned char *count);
void update_duty_ratio(struct MotorDrive *motor);
void update_time(signed long long *time);

// global variables //
unsigned char periodCount = 0;

struct MotorDrive motor1;
struct MotorDrive motor2;
struct MotorDrive motor3;
struct MotorDrive motor4;
//MotorDrive motor1, motor2, motor3, motor4;


// software switch debounce //
unsigned int switchDebounce3 = 0x00000000; // contains the last 36 values

signed long long maxLowTimer = 2 * bounceTime;
signed int timeRange = 0;

signed int currentTime1 = 0;
signed int currentTime2 = 0;
signed int currentTime3 = 0;
signed int currentTime4 = 0;
signed long long previousTime1 = 0;
signed long long previousTime2 = 0;
signed long long previousTime3 = 0;
signed long long previousTime4 = 0;
unsigned char IOC1flag = 0;
unsigned char IOC2flag = 0;
unsigned char IOC3flag = 0;
unsigned char IOC4flag = 0;
unsigned char IOCcount1 = 0;
unsigned char IOCcount2 = 0;
unsigned char IOCcount3 = 0;
unsigned char IOCcount4 = 0;

// UART chars //
unsigned char recievedCharacterFlag = 0;
unsigned char recievedCharacter1 = 0;
unsigned char recievedCharacter2 = 0;
unsigned char whichChar = 0;

unsigned char beginOperationFlag = 0;

void __interrupt() interruptServiceRoutine(void){
    if (PIR3bits.RC1IF == 1){
        if (whichChar == 0) { recievedCharacter1 = RC1REG; whichChar++; }
        else { LATCbits.LATC6 = 1; recievedCharacter2 = RC1REG; whichChar = 2; }
        recievedCharacterFlag = 1;
    }
    if (PIR0bits.TMR0IF == 1){
        if (periodCount % 4 == 0){
            update_time(&previousTime1);
            update_time(&previousTime2);
            update_time(&previousTime3);
            update_time(&previousTime4);
        }
        periodCount++;
        if (motor1.dutyRatio < periodCount) { LATBbits.LATB7 = 0; }
        else { LATBbits.LATB7 = 1; }
        if (motor2.dutyRatio < periodCount) { LATBbits.LATB6 = 0; }
        else { LATBbits.LATB6 = 1; }
        if (motor3.dutyRatio < periodCount) { LATBbits.LATB5 = 0; }
        else { LATBbits.LATB5 = 1; }
        if (motor4.dutyRatio < periodCount) { LATBbits.LATB4 = 0; }
        else { LATBbits.LATB4 = 1; }
        
        if (periodCount == timePeriod) { periodCount = 0; }
        PIR0bits.TMR0IF = 0;
    }
    if (PIR0bits.IOCIF == 1){
        //PORTCbits.RC7 = !PORTCbits.RC7;
        if (IOCCFbits.IOCCF3 == 1 ){
            IOC4flag = 1;
            IOCCFbits.IOCCF3 = 0;
        }
        if (IOCCFbits.IOCCF2 == 1){
            IOC3flag = 1;
            IOCCFbits.IOCCF2 = 0;
        }
        if (IOCCFbits.IOCCF1 == 1){
            IOC2flag = 1;
            IOCCFbits.IOCCF1 = 0;
        }
        if (IOCCFbits.IOCCF0 == 1){
            IOC1flag = 1;
            IOCCFbits.IOCCF0 = 0;
        }
        PIR0bits.IOCIF = 0;
    }
    INTCONbits.GIE = 1;
}

void main(void) {
    // remove watchdog timer because its a fucking cunt
    //WDTCON0.SWDTEN = 0;
    
    // initiate motor drives
    // motor 1 - RB7 //
    motor1.dutyRatio = 0;
    motor1.currentOrientation = 0;
    motor1.targetOrientation = 7;
    update_duty_ratio(&motor1);
    // motor 2 - RB6 //
    motor2.dutyRatio = 0;
    motor2.currentOrientation = 0;
    motor2.targetOrientation = 0;
    update_duty_ratio(&motor2);
    // motor 3 - RB5 //
    motor3.dutyRatio = 0;
    motor3.currentOrientation = 0;
    motor3.targetOrientation = 0;
    update_duty_ratio(&motor3);
    // motor 4 - RB4 //
    motor4.dutyRatio = 0;
    motor4.currentOrientation = 0;
    motor4.targetOrientation = 0;
    update_duty_ratio(&motor4);
    
    // initiate interrupts
    INTCONbits.GIE = 1;
    INTCONbits.PEIE = 1;
    INTCONbits.INTEDG = 0;
    
    // define register A //
    TRISA = 0b00000110;      // A1 and A2 as inputs
    ANSELA = 0x00;
    WPUA = 0x00;
    INLVLA = 0x00;
    SLRCONA = 0x00;
    ODCONA = 0x00;
    PORTA = 0x00;       
    
    // define output register B
    TRISB = 0b00000000;
    ANSELB = 0b00000000;
    WPUB = 0b00000000;
    INLVLB = 0b00000000;
    SLRCONB = 0b00000000;
    ODCONB = 0x00;
    PORTB = 0x00;
    
    // enable input register C
    TRISC = 0b00101111;
    ANSELC = 0b00000000;
    WPUC = 0b00001111;
    INLVLC = 0b00000000;
    SLRCONC = 0b00000000;
    ODCONC = 0x00;
    PORTC = 0x00;
    RX1DTPPS = 0x15;        // route UART reciever to C5
    //TX1CKPPS = 0x14;        // route UART transmitter to C4
    RC4PPS = 0x0F;
    
    
    // define timer
    timer_0.T0EN = 1;
    timer_0.T016BIT = 0;
    timer_0.T0OUTPS = 0b1111;   // 1:1 ratio
    T0CON1bits.T0CS = 0b010;
    T0CON1bits.T0ASYNC = 0;
    T0CON1bits.T0CKPS = 0b0001; // 1:1 ratio
    TMR0H = 0b10101000;         // count before reset
    
    // setup timer 2 for PWM
    /*
    T2CLKCONbits.CS = 0b0011;   // use 32MHz clock
    T2CONbits.CKPS = 0b000;     // 1:1 prescaler
    T2HLTbits.PSYNC = 0;        // not synced with fosc/4
    T2HLTbits.CKPOL = 0;        // rising edge
    T2HLTbits.CKSYNC = 1;       // output bit synced with timer
    T2HLTbits.MODE = 0b00000;   // regular timer - software control?
    T2RSTbits.RSEL = 0b0000;    // T2INPPS ?
    */
    
    // setup for UART communication //
    SP1BRGL = 0b00110011;   // 51 baud rate thing
    SP1BRGH = 0x00;
    TX1STAbits.BRGH = 0;
    BAUD1CONbits.BRG16 = 0;
    
    RC1STAbits.CREN = 1;
    TX1STAbits.SYNC = 0;
    RC1STAbits.SPEN = 1;
    TX1STAbits.TXEN = 1;
    
    // complete rotations bit //
    unsigned char completedRotations = 0;
    
    // initialise IOC interrupts //
    IOCCP = 0x00;
    IOCCN = 0x0F;
    IOCCF = 0x0F; 
    
    
   
    PIE3bits.RC1IE = 1;
    PIR0bits.TMR0IF = 0;
    PIE0bits.TMR0IE = 1;
    PIE0bits.IOCIE = 1;
    while(1){
        if (beginOperationFlag == 1){
            completedRotations = (motor1.targetOrientation == motor1.currentOrientation) &
                                 (motor2.targetOrientation == motor2.currentOrientation) &
                                 (motor3.targetOrientation == motor3.currentOrientation) &
                                 (motor4.targetOrientation == motor4.currentOrientation);
            if ((completedRotations == 1) & (PORTAbits.RA2 == 1)){
                LATCbits.LATC7 = 1;
                LATCbits.LATC6 = 0;
                beginOperationFlag = 0;
            }
        }
        if (whichChar == 2){
            //motor2.targetOrientation = 7;
            
            if (PORTAbits.RA1 == 1) {
                motor1.targetOrientation = (recievedCharacter1 & 0b00111000) >> 3;
                update_duty_ratio(&motor1);
                motor2.targetOrientation = (((recievedCharacter1 & 0b00000001) << 2) OR ((recievedCharacter1 & 0b00000100) >> 2) OR ((recievedCharacter1 & 0b00000010))); // flip character
                update_duty_ratio(&motor2);
                motor3.targetOrientation = (recievedCharacter2 & 0b00111000) >> 3;
                update_duty_ratio(&motor3);
                motor4.targetOrientation = ((recievedCharacter2 & 0b00000001) << 2) OR ((recievedCharacter2 & 0b00000100) >> 2) OR ((recievedCharacter2 & 0b00000010));
                update_duty_ratio(&motor4);
                beginOperationFlag = 1;
            } else{
                TX1REG = recievedCharacter1;
                TX1REG = recievedCharacter2;
                // set own done bit low
                LATCbits.LATC6 = 0;
            }
            whichChar = 0;
            recievedCharacterFlag = 0;
        }
        if (IOC1flag == 1){
            currentTime1 = TMR0L;
            if (currentTime1 - previousTime1 > bounceTime){
                previousTime1 = currentTime1;
                increment_rotation(&motor1, &IOCcount1);
                update_duty_ratio(&motor1);
                }
            IOC1flag = 0;
        }
        if (IOC2flag == 1){
            currentTime2 = TMR0L;
            if (currentTime2 - previousTime2 > bounceTime){
                previousTime2 = currentTime2;
                increment_rotation(&motor2, &IOCcount2);
                update_duty_ratio(&motor2);
                }
            IOC2flag = 0;
        }
        if (IOC3flag == 1){
            currentTime3 = TMR0L;
            if (currentTime3 - previousTime3 > bounceTime){
                previousTime3 = currentTime3;
                increment_rotation(&motor3, &IOCcount3);
                update_duty_ratio(&motor3);
                }
            IOC3flag = 0;
        }
        if (IOC4flag == 1){
            currentTime4 = TMR0L;
            if (currentTime4 - previousTime4 > bounceTime){
                previousTime4 = currentTime4;
                increment_rotation(&motor4, &IOCcount4);
                update_duty_ratio(&motor4);
                }
            IOC4flag = 0;
        }
    }
    return;
}

    
void increment_rotation(struct MotorDrive *motor, unsigned char *count){
    motor->currentOrientation++;
    if (motor->currentOrientation > maximumRotation){ motor->currentOrientation = 0;}
    return;
}

void update_duty_ratio(struct MotorDrive *motor){
    signed char range = motor->targetOrientation - motor->currentOrientation;
    if ((range==0)||((range<1)&&(range>0))||((range<0)&&(range+1+maximumRotation<1))){
        motor->dutyRatio = 0;
    } else if (((range<slowDistance)&&(range>0))||((range<0)&&(range+1+maximumRotation<slowDistance))){
        motor->dutyRatio = slowDR;
    } else {
        motor->dutyRatio = fastDR;
    }
    return;
}

void update_time(signed long long *time){
    *time -= TMR0H;
    if (*time < -maxLowTimer) { *time = -maxLowTimer; }
    return;
}